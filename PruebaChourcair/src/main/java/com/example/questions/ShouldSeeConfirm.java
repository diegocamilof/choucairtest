package com.example.questions;

import com.example.ui.UTestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.ensure.Ensure;

public class ShouldSeeConfirm implements Question<Boolean> {

    private String text;

    public ShouldSeeConfirm(String text) {
        this.text = text;
    }

    public static ShouldSeeConfirm Text(String text){
        return new ShouldSeeConfirm(text);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean resp = false;
        if (UTestPage.CONFIRMATION_TEST.resolveFor(actor).getText().equals(text)){
            actor.attemptsTo(
                    Ensure.that(UTestPage.CONFIRMATION_TEST).hasText(text)
            );
            resp = true;
        }
        return resp;
    }
}
