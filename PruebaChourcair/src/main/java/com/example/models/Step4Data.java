package com.example.models;

public class Step4Data {
 private String password,confirm_password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public Step4Data(String password, String confirm_password) {
        this.password = password;
        this.confirm_password = confirm_password;
    }
}
