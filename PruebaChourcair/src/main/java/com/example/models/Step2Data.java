package com.example.models;

public class Step2Data {
    private String city,postal_code,country;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Step2Data(String city, String postal_code, String country) {
        this.city = city;
        this.postal_code = postal_code;
        this.country = country;
    }
}
