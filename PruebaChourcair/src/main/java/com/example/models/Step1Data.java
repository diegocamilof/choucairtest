package com.example.models;

public class Step1Data {
    private String first_name,last_name,email,date_of_birth,language_spoken;
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getLanguage_spoken() {
        return language_spoken;
    }

    public void setLanguage_spoken(String language_spoken) {
        this.language_spoken = language_spoken;
    }

    public Step1Data(String first_name, String last_name, String email, String date_of_birth, String language_spoken) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.date_of_birth = date_of_birth;
        this.language_spoken = language_spoken;
    }
}
