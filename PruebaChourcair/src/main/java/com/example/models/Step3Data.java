package com.example.models;

public class Step3Data {
    private String your_computer,version,language,mobile_device,model,os;

    public String getYour_computer() {
        return your_computer;
    }

    public void setYour_computer(String your_computer) {
        this.your_computer = your_computer;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMobile_device() {
        return mobile_device;
    }

    public void setMobile_device(String mobile_device) {
        this.mobile_device = mobile_device;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Step3Data(String your_computer, String version, String language, String mobile_device, String model, String os) {
        this.your_computer = your_computer;
        this.version = version;
        this.language = language;
        this.mobile_device = mobile_device;
        this.model = model;
        this.os = os;
    }
}
