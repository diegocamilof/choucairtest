package com.example.tasks;

import com.example.models.Step3Data;
import com.example.models.Step4Data;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;
import java.util.Map;

import static com.example.ui.Step4Page.*;

public class Step4 implements Task {
    DataTable data;

    public Step4(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        Step4Data stepdata = new Step4Data(a.get(0).get("password"),a.get(0).get("confirm_password"));
        actor.attemptsTo(
                Enter.theValue(stepdata.getPassword()).into(PASSWORD),
                Enter.theValue(stepdata.getConfirm_password()).into(COMFIRM_PASSWORD),
                Click.on(TERM_OF_USE),
                Click.on(PRIVACY_SETTINGS),
                Click.on(BTN_COMPLETE_SETUP)
        );
    }
    public static Step4 FillData(DataTable data){
        return Tasks.instrumented(Step4.class, data);
    }
}



