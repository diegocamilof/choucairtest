package com.example.tasks;

import com.example.models.Step3Data;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.ScrollTo;
import net.serenitybdd.screenplay.waits.Wait;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.Map;

import static com.example.ui.Step3Page.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class Step3 implements Task {
    DataTable data;

    public Step3(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        Step3Data stepdata = new Step3Data(a.get(0).get("your_computer"),a.get(0).get("version"),a.get(0).get("language"),a.get(0).get("mobile_device"),a.get(0).get("model"),a.get(0).get("OS"));
        actor.attemptsTo(
                Click.on(COMPUTER),
                Enter.theValue(stepdata.getYour_computer()).into(SELECT_COMPUTER).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(VERSION),
                Enter.theValue(stepdata.getVersion()).into(SELECT_VERSION).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(LANGUAGE),
                Enter.theValue(stepdata.getLanguage()).into(SELECT_LANGUAGE).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                WaitUntil.the(MOBILE_DEVICE,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(MOBILE_DEVICE),
                Scroll.to(SELECT_MOBILE_DEVICE.of(stepdata.getMobile_device())),
                Click.on(SELECT_MOBILE_DEVICE.of(stepdata.getMobile_device())),
                Click.on(MODEL),
                Scroll.to(SELECT_MODEL.of(stepdata.getModel())),
                Click.on(SELECT_MODEL.of(stepdata.getModel())),
                Click.on(OS),
                Scroll.to(SELECT_OS.of(stepdata.getOs())),
                Click.on(SELECT_OS.of(stepdata.getOs())),
                Click.on(BTN_LAST_STEP)
        );
    }
    public static Step3 FillData(DataTable data){
        return Tasks.instrumented(Step3.class, data);
    }
}



