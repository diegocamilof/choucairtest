package com.example.tasks;

import com.example.models.Step2Data;
import com.example.utils.Wait;
import io.cucumber.datatable.DataTable;
import static com.example.ui.Step2Page.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.Map;

import static com.example.ui.UTestPage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class Step2 implements Task {
    DataTable data;

    public Step2(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        Step2Data stepdata = new Step2Data(a.get(0).get("city"),a.get(0).get("postal_code"),a.get(0).get("country"));
        actor.attemptsTo(
                Enter.theValue("").into(CITY),
                Enter.theValue("").into(ZIP),
                Enter.theValue(stepdata.getCity()).into(CITY),
                Hit.the(Keys.ARROW_DOWN, Keys.ENTER).into(CITY),
                Enter.theValue(stepdata.getPostal_code()).into(ZIP).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(COUNTRY),
                Enter.theValue(stepdata.getCountry()).into(SELECT_COUNTRY),
                Hit.the(Keys.ARROW_DOWN, Keys.ENTER).into(SELECT_COUNTRY),
                Click.on(BTN_NEXT_DEVICES)
        );
    }
    public static Step2 FillData(DataTable data){
        return Tasks.instrumented(Step2.class, data);
    }
}



