package com.example.tasks;

import static com.example.ui.UTestPage.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.InstrumentedTask;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class ClickTo implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_JOIN_TODAY)
        );
    }
    public static ClickTo TheButton(){
        return Tasks.instrumented(ClickTo.class);
    }
}
