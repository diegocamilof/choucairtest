package com.example.tasks;

import static com.example.ui.Step1Page.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

import com.example.models.Step1Data;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.eo.Se;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.steps.stepdata.StepData;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class Step1 implements Task {
    DataTable data;

    public Step1(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);

        Step1Data stepdata= new Step1Data(a.get(0).get("first_name"),a.get(0).get("last_name"),a.get(0).get("email"),a.get(0).get("date_of_birth"),a.get(0).get("language_spoken"));
        String mail = stepdata.getEmail();
        int random = (int) (Math.random() * 10000);
        String emailComplete = mail + random + "@gmail.com";
        actor.attemptsTo(
                WaitUntil.the(FIRST_NAME,isPresent()).forNoMoreThan(15).seconds(),
                Enter.theValue(stepdata.getFirst_name()).into(FIRST_NAME),
                Enter.theValue(stepdata.getLast_name()).into(LAST_NAME),
                Enter.theValue(emailComplete).into(EMAIL)
        );
        String date = stepdata.getDate_of_birth();
        String[] datesplit = date.split("/");
        actor.attemptsTo(
                Click.on(MONTH),
                Click.on(SELECT_MONTH.of(datesplit[0])),
                Click.on(DAY),
                Click.on(SELECT_DAY.of(datesplit[1])),
                Click.on(YEAR),
                Click.on(SELECT_YEAR.of(datesplit[2])),
                Click.on(SELECT_LANGUAGE),
                Click.on(SELECTED_LANGUAGE.of(stepdata.getLanguage_spoken())),
                Click.on(BTN_NEXT_LOCATION)
        );
    }
    public static Step1 FillData(DataTable data){
        return Tasks.instrumented(Step1.class, data);
    }
}



