package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Step2Page {
    public static final Target CITY = Target.the("City").located(By.id("city"));
    public static final Target ZIP = Target.the("Postal code").located(By.id("zip"));
    public static final Target COUNTRY = Target.the("Country").located(By.cssSelector("div[name='countryId']"));
    public static final Target SELECT_COUNTRY = Target.the("Select country").located(By.cssSelector("input[placeholder='Select a country']"));
    public static final Target BTN_NEXT_DEVICES = Target.the("Next devices button").located(By.xpath("//a[@class='btn btn-blue pull-right'] //span[.='Next: Devices']"));
}
