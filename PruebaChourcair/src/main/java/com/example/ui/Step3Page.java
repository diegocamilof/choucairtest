package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Step3Page {
    public static final Target COMPUTER = Target.the("Your computer").located(By.cssSelector("div[name='osId'] > div[placeholder='Select OS'] > span.btn-default"));
    public static final Target SELECT_COMPUTER = Target.the("Select your computer").located(By.cssSelector("div[name='osId'] > input[placeholder='Select OS']"));
    public static final Target VERSION = Target.the("Version").located(By.xpath("//span[@class='btn btn-default form-control ui-select-toggle']// span[.='Select a Version']"));
    public static final Target SELECT_VERSION = Target.the("Select version").located(By.cssSelector("input[placeholder='Select a Version']"));
    public static final Target LANGUAGE = Target.the("Language").located(By.xpath("//span[@class='btn btn-default form-control ui-select-toggle'] // span[.='Select OS language']"));
    public static final Target SELECT_LANGUAGE = Target.the("Select language").located(By.cssSelector("input[placeholder='Select OS language']"));
    public static final Target MOBILE_DEVICE = Target.the("Your mobile device").located(By.xpath("//span[@class='btn btn-default form-control ui-select-toggle'] // span[.='Select Brand']"));
    public static final Target SELECT_MOBILE_DEVICE = Target.the("Select mobile device").locatedBy("//div[.='{0}']");
    public static final Target MODEL = Target.the("Model").located(By.xpath("//span[@class='btn btn-default form-control ui-select-toggle'] // span[.='Select a Model']"));
    public static final Target SELECT_MODEL = Target.the("Select model").locatedBy("//div[.='{0}']");
    public static final Target OS = Target.the("Operating System").located(By.cssSelector("div[name='handsetOSId'] > div[placeholder='Select OS'] > span.btn-default"));
    public static final Target SELECT_OS = Target.the("Select OS").locatedBy("//div[.='{0}']");
    public static final Target BTN_LAST_STEP = Target.the("Last Step Button").located(By.xpath("//a[@class='btn btn-blue pull-right']// span[.='Next: Last Step']"));
    public static final Target PASSWORD = Target.the("Password").located(By.id("#password"));
    public static final Target COMFIRM_PASSWORD = Target.the("Comfirm password").located(By.id("#confirmPassword"));
    public static final Target TERM_OF_USE = Target.the("Term of use").located(By.id("#termOfUse"));
    public static final Target PRIVACY_SETTINGS = Target.the("Privacy settings").located(By.id("#privacySetting"));
    public static final Target BTN_COMPLETE_SETUP = Target.the("Complete setup Button").located(By.xpath("//a[@class='btn btn-blue']// span[.='Complete Setup']"));
}
