package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Step1Page {
    public static final Target FIRST_NAME = Target.the("First Name").located(By.id("firstName"));
    public static final Target LAST_NAME = Target.the("Last name").located(By.id("lastName"));
    public static final Target EMAIL = Target.the("Email address").located(By.id("email"));
    public static final Target MONTH = Target.the("Month").located(By.id("birthMonth"));
    public static final Target SELECT_MONTH = Target.the("Select month").locatedBy("//select[@id='birthMonth'] //option[text()='{0}']");
    public static final Target DAY = Target.the("Day").located(By.cssSelector("#birthDay"));
    public static final Target SELECT_DAY = Target.the("Select day").locatedBy("//select[@id='birthDay'] //option[text()='{0}']");
    public static final Target YEAR = Target.the("Year").located(By.cssSelector("#birthYear"));
    public static final Target SELECT_YEAR = Target.the("Select Year").locatedBy("//select[@id='birthYear']//option[.='{0}']");
    public static final Target SELECT_LANGUAGE= Target.the("Select language").located(By.cssSelector("#languages > div > input"));
    public static final Target SELECTED_LANGUAGE = Target.the("Seleccionar lenguaje").locatedBy("//div[.='{0}']");
    public static final Target BTN_NEXT_LOCATION = Target.the("Next location button").located(By.xpath("//a[@class='btn btn-blue']//span[.='Next: Location']"));

}
