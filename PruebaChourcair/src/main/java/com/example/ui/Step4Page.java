package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Step4Page {
    public static final Target PASSWORD = Target.the("Password").located(By.id("password"));
    public static final Target COMFIRM_PASSWORD = Target.the("Comfirm password").located(By.id("confirmPassword"));
    public static final Target TERM_OF_USE = Target.the("Term of use").located(By.id("termOfUse"));
    public static final Target PRIVACY_SETTINGS = Target.the("Privacy settings").located(By.id("privacySetting"));
    public static final Target BTN_COMPLETE_SETUP = Target.the("Complete setup Button").located(By.xpath("//a[@class='btn btn-blue']// span[.='Complete Setup']"));
}
