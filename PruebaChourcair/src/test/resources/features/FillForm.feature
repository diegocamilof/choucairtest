Feature: The user wants to fill the form in the UTest page
  @smoke
  Scenario Outline: Fill form
    Given The user has been sent to the Test page
    When The user clicks button Join Today
    And The user has been fill all data in the step 1 with the next data:
      | first_name   | last_name   | email   | date_of_birth   | language_spoken   |
      | <first_name> | <last_name> | <email> | <date_of_birth> | <language_spoken> |
    And The user has been fill all data in the step 2 with the next data:
      | city   | postal_code   | country   |
      | <city> | <postal_code> | <country> |
    And The user has been fill all data in the step 3 with the next data:
      | your_computer   | version   | language   | mobile_device   | model   | OS   |
      | <your_computer> | <version> | <language> | <mobile_device> | <model> | <OS> |
    And The user has been fill all data in the step 4 with the next data:
      | password   | confirm_password   |
      | <password> | <confirm_password> |
    Then The user should see the text "Welcome to the world's largest community of freelance software testers!"

    Examples:
      | first_name | last_name | email   | date_of_birth  | language_spoken | city          | postal_code | country  | your_computer | version | language | mobile_device | model   | OS           | password            | confirm_password    |
      | Diego      | Fernandez | prueba  | May/23/1995    | Spanish         | Bogotá        | 110541      | Colombia | Windows       | 10      | Spanish  | Motorola      | Moto X  | Android 5.2  | PruebaChourcair2021 | PruebaChourcair2021 |