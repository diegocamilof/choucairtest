package com.example.runners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        glue = {"com.example.stepDefinitions"},
        features = {"src/test/resources/features/FillForm.feature"},
        tags = "@smoke",
        snippets = SnippetType.UNDERSCORE
)

public class FillFormRunner {

}
