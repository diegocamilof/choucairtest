package com.example.stepDefinitions;

import com.example.models.Step1Data;
import com.example.questions.ShouldSeeConfirm;
import com.example.tasks.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;

import javax.xml.crypto.Data;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class FillFormStepDefinition {
    @Before
    public void setTheStage(){
        OnStage.setTheStage(new OnlineCast());
    }
    @Given("The user has been sent to the Test page")
    public void The_user_go_to_page(){
        theActorCalled("user").attemptsTo(GoTo.TheURL());
    }
    @When("The user clicks button Join Today")
    public void the_user_clicks_Join_Today(){
        theActorInTheSpotlight().attemptsTo(ClickTo.TheButton());
    }
    @And("The user has been fill all data in the step 1 with the next data:")
    public void step1(DataTable data){
        theActorInTheSpotlight().attemptsTo(Step1.FillData(data));
    }
    @And("The user has been fill all data in the step 2 with the next data:")
    public void step2(DataTable data){
        theActorInTheSpotlight().attemptsTo(Step2.FillData(data));
    }
    @And("The user has been fill all data in the step 3 with the next data:")
    public void step3(DataTable data){
        theActorInTheSpotlight().attemptsTo(Step3.FillData(data));
    }
    @And("The user has been fill all data in the step 4 with the next data:")
    public void step4(DataTable data){
        theActorInTheSpotlight().attemptsTo(Step4.FillData(data));
    }
   @Then("The user should see the text {string}")
    public void question1(String text){
        theActorInTheSpotlight().should(seeThat(ShouldSeeConfirm.Text(text)));
   }


}
